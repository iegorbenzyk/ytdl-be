const dbConfig = require("../../config/db.config");

const mongoose = require("mongoose");

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.videoInfo = require("./video-info.model.js")(mongoose);

module.exports = db;

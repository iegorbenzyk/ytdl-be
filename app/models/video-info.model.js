module.exports = mongoose => {
    const VideoInfo = mongoose.model(
        "VideoInfo",
        mongoose.Schema(
            {
                title: String,
                url: String,
                channel: String,
                img: String
            },
            { timestamps: true }
        )
    );

    return VideoInfo;
};

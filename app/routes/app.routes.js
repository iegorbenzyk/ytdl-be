module.exports = app => {
    const logs = require("../controllers/app.controller");

    let router = require("express").Router();

    router.post("/download", logs.create);

    router.get("/list", logs.findAll);

    app.use("/api", router);
}
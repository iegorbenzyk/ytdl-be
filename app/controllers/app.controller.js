const db = require("../models");
const ytdl = require("ytdl-core");
const cp = require('child_process');
const ffmpeg = require('ffmpeg-static');
const VideoInfo = db.videoInfo;

exports.create = async (req, res) => {
    if (!req.body.url) {
        res.status(400).send({message: "Content can not be empty!"});
        return;
    }
    const url = req.body.url;
    const info = await ytdl.getInfo(url);
    const header = 'attachment; filename="video_from_youtube.mkv"'
    res.setHeader("Content-Disposition", header);

    const audio = ytdl(url, { quality: 'highestaudio' });
    const video = ytdl(url, { quality: 'highestvideo' });
    const ffmpegProcess = cp.spawn(ffmpeg, [
        // Remove ffmpeg's console spamming
        '-loglevel', '8', '-hide_banner',
        // Set inputs
        '-i', 'pipe:3',
        '-i', 'pipe:4',
        // Map audio & video from streams
        '-map', '0:a',
        '-map', '1:v',
        // Keep encoding
        '-c:v', 'copy',
        // Define output file
        '-f', 'matroska', 'pipe:5',
    ], {
        windowsHide: true,
        stdio: [
            /* Standard: stdin, stdout, stderr */
            'inherit', 'inherit', 'inherit',
            /* Custom: pipe:3, pipe:4, pipe:5 */
            'pipe', 'pipe', 'pipe',
        ],
    });
    audio.pipe(ffmpegProcess.stdio[3]);
    video.pipe(ffmpegProcess.stdio[4]);
    ffmpegProcess.stdio[5].pipe(res);

    const filter = {title: info.videoDetails.title};
    const vInfo = {
        title: info.videoDetails.title,
        channel: info.videoDetails.author.name,
        img: info.videoDetails.thumbnails[0].url,
        url: url,
    };
    await VideoInfo.findOneAndUpdate(filter, vInfo, {upsert: true});
};

exports.findAll = async (req, res) => {
    try {
        const data = await VideoInfo.find({}).sort('-updatedAt');
        res.send(data);
    } catch (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving data."
        })
    }
};
